#!/usr/bin/env python3


# Program to find words in a crossword puzzle
# From enlighten programming challenge
# https://gitlab.com/enlighten-challenge/alphabet-soup

# Only able to handle puzzles following this rule:
#   each word should occur exactly once (if there are multiple, only one is found)

# The program is case-sensitive by default, set 'caseSensitive = False' to make case-insensitive
#   The declaration of 'caseSensitive' is located on line 81 (at beginning of 'parse_input_string')

# Created by:   Josh Nelson
# Started:      Oct 21, 2023
# Finished:     Oct 24, 2023


import sys


# Main function
def main(argv):

    # Sets file name and makes sure there is the right number of arguments
    if len(argv) == 1:
        fileName = argv[0]
    elif len(argv) == 0:
        fileName = input("File: ")

    # If there is the wrong number of arguments, print correct usage and close program
    else:
        print("Usage: <python | python3> alphabet-soup.py [input.txt]")
        return 1

    # Try to open file; if it can't be opened, close program
    try:
        file = open(fileName, 'r')

    except:
        print("Error: could not open file \'" + fileName + '\'')
        print("exiting program...")
        return 2

    # Import file contents and close it
    fileContents = file.read()
    file.close()


    # In the README it says "You may assume that the input files are correctly formatted. 
    #                           Error handling for invalid input files may be ommitted."
    # Therefore, there is no error handling for files formatted incorrectly


    # Get needed information from 'fileContents'
    board, words = parse_input_string(fileContents)

    # Iterate through all words
    for wordNum in range( len(words) ):

        wordStarts = index_2d(board, words[wordNum][0][0])
        foundWord, wordStart, wordFinish = find_word(board, words[wordNum][0], wordStarts)

        # If a word is found, print it out
        if foundWord:
            print(words[wordNum][1] + ' '  + str(wordStart[0]) + ':'  + str(wordStart[1])+ ' '  + str(wordFinish[0]) + ':' + str(wordFinish[1]))
        
        # If not, print out the word followed by ' not found'
        else:
            print(words[wordNum][1] + " not found")
    
    return 0

# End of main


# Parse input string that was read from file
def parse_input_string(inputString):
    
    # The program is case-sensitive by default, set 'caseSensitive = False' to make case-insensitive
    caseSensitive = True

    # Split input by newline character
    inputString = inputString.split('\n')

    # The size is inputString[0] and is formatted as < rows >x< columns >
    size = [int(s) for s in inputString[0].split('x') ]

    # Initialize board to array of 0 chars
    board = [['0'] * size[1] for _ in range(size[0])]

    # Import board
    for r in range(size[0]):

        # Set row for inputString
        boardRow = r + 1

        # Assign board to character array, splitting rows by whitespace
        board[r] = inputString[boardRow].split()

        # The program is case-sensitive by default, set 'caseSensitive = False' to make case-insensitive
        if not(caseSensitive):
            board[r] = [char.upper() for char in board[r]]

    # Claculate number of words
    startIndex = size[0] + 1
    numWords = len(inputString) - startIndex

    # Initialize words to array of 0 chars
    words = [['0'] * 2 for _ in range(numWords)]

    # Import board
    for i in range(numWords):

        # The replace takes out spaces from the words for searching
        words[i][0] = inputString[startIndex + i].replace(' ', '')

        # Save original words for output
        words[i][1] = inputString[startIndex + i]

        # The program is case-sensitive by default, set 'caseSensitive = False' to make case-insensitive
        if not(caseSensitive):
            words[i] = [words[i][0].upper(), words[i][1].upper()]

    # Remove last word if it is blank
    while words[-1][0] == '':
        words.pop()

    return board, words

# End of parse_input_string


# Find word
def find_word(board, word, wordStarts):

    for wordStart in wordStarts:

        jumps = [
            [ 1,  0], # Down
            [-1,  0], # UP
            [ 0,  1], # Right
            [ 0, -1], # Left
            [ 1,  1], # Diagonal down-right
            [ 1, -1], # Diagonal down-left
            [-1,  1], # Diagonal up-right
            [-1, -1]  # Diagonal up-left
        ]

        for jump in jumps:

            wordFinish = found_word(board, word, wordStart, jump)

            # Once a word is found, exit function
            if wordFinish != ['X', 'X']:
                return True, wordStart, wordFinish

    return False, ['X', 'X'], ['X', 'X']

# End of find_word


# If the return is ['X', 'X'], no word was found
def found_word(board, word, start, jump):

    # Assuming rectangular board, size[height, width]
    size = [len(board), len(board[0])]

    # If the direction 'jump' does not have enough room for word, exit function
    if not( jump_good(size, len(word), start, jump) ):
        return ['X', 'X']

    # Iterate through chars of word, exiting if there is a different char
    for charNum in range( len(word) ):

        wordChar = board[start[0] + charNum * jump[0]][start[1] + charNum * jump[1]]

        # Exit if the char from the word is different than the one on the board
        if word[charNum] != wordChar:

            return ['X', 'X']

    # Return the ending index
    return [start[0] + ( len(word) - 1 ) * jump[0], start[1] + ( len(word) - 1 ) * jump[1]]

# End of find_word


# Find if there is enough space for there to be a word starting from 'start' going in direction 'jump'
def jump_good(size, length, start, jump):

    # Down
    down = length <= size[0] - start[0]
    if jump == [1, 0]:
        if not(down):
            return False
    
    # Up
    up =  length <= start[0] + 1
    if jump == [-1, 0]:
        if not(up):
            return False
    
    # Right
    right = length <= size[1] - start[1]
    if jump == [0, 1]:
        if not(right):
            return False
    
    # Left
    left = length <= start[1] + 1
    if jump == [0, -1]:
        if not(left):
            return False
    
    # Diagonal down-right
    if jump == [1, 1]:
        if not(down and right):
            return False
    
    # Diagonal down-left
    if jump == [1, -1]:
        if not(down and left):
            return False
    
    # Diagonal up-right
    if jump == [-1, 1]:
        if not(up and right):
            return False
    
    # Diagonal up-left
    if jump == [-1, -1]:
        if not(up and left):
            return False
    
    return True

# End of jump_good


# Find all instances of 'v' in 'myList'
# Modified from: https://stackoverflow.com/a/5775397
def index_2d(myList, v):

    arr = []

    for i, x in enumerate(myList):
        for j, y in enumerate(x):
            if y == v:
                arr.append([i, j])
    
    return arr

# End of index_2d


# End of file call to Main
if __name__ == "__main__":
    sys.exit( main(sys.argv[1:]) )
